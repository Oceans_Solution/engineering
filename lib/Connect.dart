
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Connect {
  static const NOT_CONNECTED = 0;
  static const MOBILE = 1;
  static const WIFI = 2;

  Future<int> checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if(connectivityResult == ConnectivityResult.mobile) {
      return Connect.MOBILE;
    } else if(connectivityResult == ConnectivityResult.wifi) {
      return Connect.WIFI;
    } else if(connectivityResult == ConnectivityResult.none) {
      return Connect.NOT_CONNECTED;
    } else {
      return Connect.NOT_CONNECTED;
    }
  }

  void setToast(message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 14.0
    );
  }
}