import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'Connect.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.storage.request();
  await Permission.photos.request();
  await Permission.microphone.request(); // if you need microphone permission
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        backgroundColor: Colors.black.withOpacity(0.5),
      ),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // 풀스크린
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [SystemUiOverlay.bottom]);
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setEnabledSystemUIOverlays([]);

    // 인터넷 연결여부
    checkInternet(context);

    Timer(
      Duration(seconds: 3),
          () =>
          Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(builder: (context) => MyHomePage()), (
                  route) => false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset('assets/splash.png'),
      ),
    );
  }

  Future<void> checkInternet(BuildContext context) async {
    var connectStatus = await Connect().checkConnection();
    if(connectStatus == Connect.NOT_CONNECTED) {
      Connect().setToast("인터넷 연결상태가 원활하기 않습니다.");
      Timer(
        Duration(seconds: 1),
            () => SystemNavigator.pop(),//(앱 종료)
      );

    }
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final int FINISH_INTERVAL_TIME = 2000;
  var _backPressedTime = 0;
  //late WebViewController _controller;

  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  late PullToRefreshController pullToRefreshController;

  @override
  void initState() {
    super.initState();
    //if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Stack(
                children: [
                  InAppWebView(
                    key: webViewKey,
                    initialUrlRequest:
                    URLRequest(url: Uri.parse("http://oilpress.co.kr/")),
                    initialOptions: options,
                    pullToRefreshController: pullToRefreshController,
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    onLoadStart: (controller, url) {
                      setState(() {});
                    },
                    androidOnPermissionRequest:
                        (controller, origin, resources) async {
                      return PermissionRequestResponse(
                          resources: resources,
                          action: PermissionRequestResponseAction.GRANT);
                    },
                    onUpdateVisitedHistory: (controller, url, androidIsReload) {
                      setState(() {
                        print('onUpdateVisitedHistory');
                      });
                    },
                    onConsoleMessage: (controller, consoleMessage) {
                      print(consoleMessage);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),*/
      body: Builder(builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            setState(() {
              var tempTime = DateTime.now().millisecondsSinceEpoch;
              var intervalTime = tempTime - _backPressedTime;
              var future = webViewController!.canGoBack();
              if(intervalTime>=0 && intervalTime<FINISH_INTERVAL_TIME) {
                //print('test : $intervalTime');
                //SystemNavigator.pop();
                _onBackPressed();
              } else {
                _backPressedTime = tempTime;
                future.then((canGoBack) {
                  if(canGoBack) {
                    webViewController!.goBack();
                  } else {
                    SystemNavigator.pop();
                  }
                });
              }
            });
            return Future.value(false);
          },
          child: SafeArea(
            child:InAppWebView(
              key: webViewKey,
              initialUrlRequest:
              URLRequest(url: Uri.parse("http://oilpress.co.kr/")),
              initialOptions: options,
              pullToRefreshController: pullToRefreshController,
              onWebViewCreated: (controller) {
                webViewController = controller;
              },
              onLoadStart: (controller, url) {
                // 인터넷 연결여부
                checkInternet(context);
                setState(() {});
              },
              androidOnPermissionRequest:
                  (controller, origin, resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              onConsoleMessage: (controller, consoleMessage) {
                print(consoleMessage);
              },
            ),
            /*child: WebView(
              initialUrl: 'http://oilpress.co.kr/',
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
              },
              onProgress: (int progress) {
                print("WebView is loading (progress : $progress%)");
              },
              javascriptChannels: <JavascriptChannel>{
                _toasterJavascriptChannel(context),
              },
              navigationDelegate: (NavigationRequest request) {
                if (request.url.startsWith('https://www.youtube.com/')) {
                  print('blocking navigation to $request}');
                  return NavigationDecision.prevent;
                }
                print('allowing navigation to $request');
                return NavigationDecision.navigate;
              },
              onPageStarted: (String url) {
                // 인터넷 연결여부
                checkInternet(context);
                print('Page started loading: $url');
              },
              onPageFinished: (String url) {
                print('Page finished loading: $url');
              },
              gestureNavigationEnabled: true,
            ),*/
          ),
        );
      }),
    );
  }

  Future<bool> _onBackPressed() async {
    return await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text("종료할까요?"),
        actions: <Widget>[
          FlatButton(
            child: Text("YES"),
            onPressed: () => SystemChannels.platform.invokeMethod(
                'SystemNavigator.pop'),
          ),
          FlatButton(
            child: Text("NO"),
            onPressed: ()=>Navigator.pop(context, false),
          )
        ],
      )
    );
  }

  Future<void> checkInternet(BuildContext context) async {
    var connectStatus = await Connect().checkConnection();
    if(connectStatus == Connect.NOT_CONNECTED) {
      webViewController!.loadUrl(urlRequest: URLRequest(url: Uri.parse("about:blank")));
      Connect().setToast("인터넷 연결상태가 원활하기 않습니다.");
      Timer(
        Duration(seconds: 2),
            () => SystemNavigator.pop(),//(앱 종료)
      );
    }
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}

